# A connect-4 clone

## Compilation and execution

`javac -Xlint:all -d build src/com/alexisdrai/connect4/*.java src/com/alexisdrai/util/*.java`

&&

`java -cp build com.alexisdrai.connect4.Main`

## Report (in French)

[ADRAI_connect4_report.pdf](https://drive.google.com/file/d/1NMKSm0l8m_pAN28g8jVuH_qbWeaFV-ta/view?usp=drive_link)
